package com.samplecrud.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class WishController {

	@GetMapping("/nameTestGCP")
	public String greeting(@RequestParam("name") String name, Model model) {
		String message = "Hi " + name + " welcome to GCP deployment";
		model.addAttribute("message", message);
		return "home";
	}

}