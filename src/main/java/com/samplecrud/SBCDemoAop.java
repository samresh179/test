package com.samplecrud;

import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.HandlerInterceptor;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

@Aspect
@Component
public class SBCDemoAop implements HandlerInterceptor {

	Logger log = LoggerFactory.getLogger(SBCDemoAop.class);

	private static final String CORRELATION_ID_HEADER_NAME = "X-Correlation-Id";
	private static final String CORRELATION_ID_LOG_VAR_NAME = "correlationId";
	boolean isCleared = false;

	@Pointcut(value = "execution(* com.samplecrud.*.*.*(..))")
	public void pointCutAop() {
	}

	@Around("pointCutAop()")
	public Object aopLogging(ProceedingJoinPoint pjp) {

		/*
		 * if (!isCleared) { MDC.clear(); isCleared = true; }
		 */
		Object responseData = removeKey(pjp);
		isCleared = false;
		return responseData;
	}

	public Object removeKey(ProceedingJoinPoint pjp) {

		ObjectMapper mapper = new ObjectMapper();

		mapper.enable(SerializationFeature.INDENT_OUTPUT);
		String methodName = pjp.getSignature().getName();
		String className = pjp.getSignature().getDeclaringType().getSimpleName();
		Object[] requestData = pjp.getArgs();
		Object responseData = null;
		try {
			/*
			 * if (MDC.get(CORRELATION_ID_LOG_VAR_NAME) == null) { final String
			 * correlationId = generateUniqueCorrelationId();
			 * MDC.put(CORRELATION_ID_LOG_VAR_NAME, correlationId); }
			 */
			log.info("Request Initiated " + className + " : " + methodName + "()" + "\nRequest : "
					+ mapper.writeValueAsString(requestData));
			final StopWatch timer = new StopWatch();
			timer.start();
			responseData = pjp.proceed();
			timer.stop();
			log.info("Execution time of " + className + " : " + methodName + " :: " + timer.getTotalTimeMillis()
					+ " ms");
			log.info(className + " : " + methodName + "()" + "\nResponse : " + mapper.writeValueAsString(responseData));
			return responseData;
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return responseData;
	}

	private String generateUniqueCorrelationId() {
		String crId = null;
		try {
			HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
					.getRequest();
			crId = request.getHeader(CORRELATION_ID_HEADER_NAME);
			if (crId == null) {
				crId = UUID.randomUUID().toString();
				return crId;
			}
		} catch (Exception e) {
		}
		return crId;

	}

}
